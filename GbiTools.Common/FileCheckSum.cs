﻿using Nelfias.Crypto;

namespace GbiTools.Common
{
    public class FileCheckSum
    {
        public string File { get; set; }
        public string CheckSum { get; set; }

        public string Calculate()
        {
            return SHA1.GetHashFromFile(File);
        }
    }
}