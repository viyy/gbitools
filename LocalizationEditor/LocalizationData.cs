using System;
using System.Collections.Generic;

namespace GBI.Scripts.Localization
{
    [System.Serializable]
    public class LocalizationData 
    {
        public List<LocalizationPage> Items { get; set; }= new List<LocalizationPage>();
    }

    [System.Serializable]
    public class LocalizationPage
    {
        public string Name { get; set; }
        public List<LocalizationItem> Data { get; set; } = new List<LocalizationItem>();

        public string this[string key] => Data.Find(x => x.Key == key)?.Value;
    }
    [System.Serializable]
    public class LocalizationItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}