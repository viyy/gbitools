﻿namespace LocalizationEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public ViewModel Model { get; set; } = new ViewModel();
        
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}