using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using GbiTools.Common;
using GBI.Scripts.Localization;
using LocalizationEditor.Annotations;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace LocalizationEditor
{
    public class ViewModel: INotifyPropertyChanged
    {
        #region Constants

        private const string DataPath = "LocalizationData";
        private const string RuSuffix = "_RU";
        private const string EnSuffix = "_EN";
        private const string FileExt = ".json";
        private const string PathDataExt = ".meta.txt";

        #endregion

        public ICommand NewDataCommand { get; }
        public ICommand NewPageCommand { get; }
        public ICommand NewKeyCommand { get; }
        public ICommand SaveDataCommand { get; }
        public ICommand DeletePageCommand { get; }
        public ICommand RenamePageCommand { get; }
        public ICommand DeleteKeyCommand { get; }
        public ICommand LoadDataCommand { get; }

        private WorkData _locData = new WorkData();

        public WorkData LocData
        {
            get => _locData;
            set
            {
                if (Equals(value, _locData)) return;
                _locData = value;
                OnPropertyChanged();
            }
        }

        private WorkItem _selectedItem;

        public WorkItem SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (_selectedItem==value) return;
                _selectedItem = value;
                OnPropertyChanged(nameof(SelectedItem));
            }
        }

        private WorkPage _selectedPage;
        
        public WorkPage SelectedPage
        {
            get => _selectedPage;
            set
            {
                if (_selectedPage==value) return;
                _selectedPage = value;
                OnPropertyChanged(nameof(SelectedPage));
            }
        }

        public ViewModel()
        {
            NewDataCommand = new SimpleCommand(o=>CreateNewData());
            NewPageCommand = new SimpleCommand(o=>CreateNewPage());
            NewKeyCommand = new SimpleCommand(o=>CreateNewKey(),o=>SelectedPage!=null);
            SaveDataCommand = new SimpleCommand(o=>SaveData());
            DeletePageCommand = new SimpleCommand(o=>DeletePage(),o=>SelectedPage!=null);
            RenamePageCommand = new SimpleCommand(o=>RenamePage(),o=>SelectedPage!=null);
            DeleteKeyCommand = new SimpleCommand(o=>DeleteKey(),o=>SelectedItem!=null);
            LoadDataCommand = new SimpleCommand(o=>LoadData());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region Logic

        private void CreateNewData()
        {
            LocData = new WorkData();
            SelectedPage = null;
            SelectedItem = null;
        }

        private void CreateNewPage()
        {
            LocData.Items.Add(new WorkPage{Name = "<New Page>"});
        }

        private void CreateNewKey()
        {
            SelectedPage.Data.Add(new WorkItem{Key = "<New Key>"});
        }

        private void DeletePage()
        {
            var dlg = new InputDialog
            {
                Message = $"Type DELETE to delete page '{SelectedPage.Name}'"
            };
            if (dlg.ShowDialog() == true && dlg.ResponseText == "DELETE")
            {
                LocData.Items.Remove(SelectedPage);
                SelectedPage = null;
            }
        }

        private void SaveData()
        {
            if (!Directory.Exists(DataPath))
            {
                Directory.CreateDirectory(DataPath);
            }
            var dlg = new InputDialog
            {
                Message = "Enter Name of Dictionary",
                ResponseText = LocData.CurrentFile
            };
            if (dlg.ShowDialog() == true)
            {
                var ru = new LocalizationData();
                var en = new LocalizationData();
                foreach (var page in LocData.Items)
                {
                    var ruPage = new LocalizationPage{Name = page.Name};
                    var enPage = new LocalizationPage{Name = page.Name};
                    foreach (var item in page.Data)
                    {
                        ruPage.Data.Add(new LocalizationItem{Key = item.Key, Value = item.RuText});
                        enPage.Data.Add(new LocalizationItem{Key = item.Key, Value = item.EnText});
                    }
                    ru.Items.Add(ruPage);
                    en.Items.Add(enPage);
                }

                var pathRu = Path.Combine(DataPath, dlg.ResponseText + RuSuffix + FileExt);
                var pathEn = Path.Combine(DataPath, dlg.ResponseText + EnSuffix + FileExt);
                var pathMeta = Path.Combine(DataPath, dlg.ResponseText + PathDataExt);
                File.WriteAllText(pathRu,JsonConvert.SerializeObject(ru));
                File.WriteAllText(pathEn,JsonConvert.SerializeObject(en));
                File.WriteAllLines(pathMeta, new []{pathRu, pathEn});
            }
        }

        private void RenamePage()
        {
            var dlg = new InputDialog
            {
                Message = "Enter New Name for Page",
                ResponseText = SelectedPage.Name
            };
            if (dlg.ShowDialog() == true)
            {
                SelectedPage.Name = dlg.ResponseText;
            }
            OnPropertyChanged(nameof(LocData.Items));
        }

        private void DeleteKey()
        {
            var dlg = new InputDialog
            {
                Message = $"Type DELETE to delete item '{SelectedPage.Name}/{SelectedItem.Key}'"
            };
            if (dlg.ShowDialog() == true && dlg.ResponseText == "DELETE")
            {
                SelectedPage.Data.Remove(SelectedItem);
                SelectedItem = null;
            }
        }

        private void LoadData()
        {
            if (!Directory.Exists(DataPath))
            {
                Directory.CreateDirectory(DataPath);
            }
            var dlg = new OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = PathDataExt,
                InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(),DataPath),
                Filter = "Path data|*" + PathDataExt
            };
            if (dlg.ShowDialog() == true)
            {
                var files = File.ReadAllLines(dlg.FileName);
                if (files.Length != 2)
                {
                    MessageBox.Show("Incorrect file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (!File.Exists(files[0]))
                {
                    MessageBox.Show("Rus File Not Found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!File.Exists(files[1]))
                {
                    MessageBox.Show("Eng File Not Found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                var ru = JsonConvert.DeserializeObject<LocalizationData>(File.ReadAllText(files[0]));
                var en = JsonConvert.DeserializeObject<LocalizationData>(File.ReadAllText(files[1]));
                LocData = new WorkData();
                foreach (var ruPage in ru.Items)
                {
                    var page = new WorkPage {Name = ruPage.Name};
                    var enPage = en.Items.Find(x => x.Name == ruPage.Name);
                    if (enPage!=null)
                        page.IsCorrect = true;
                    foreach (var item in ruPage.Data)
                    {
                        var enText = "";
                        enText = enPage?.Data.Find(x => x.Key == item.Key)?.Value;
                        page.Data.Add(new WorkItem{Key = item.Key, RuText = item.Value, EnText = enText});
                    }

                    if (enPage != null)
                    {
                        foreach (var item in enPage.Data)
                        {
                            if (page.Data.Any(x=>x.Key==item.Key)) continue;
                            page.Data.Add(new WorkItem{Key = item.Key, RuText = "", EnText = item.Value});
                        }
                    }

                    LocData.Items.Add(page);
                }

                foreach (var enPage in en.Items)
                {
                    if (LocData.Items.Any(x=>x.Name==enPage.Name)) continue;
                    var page = new WorkPage {Name = enPage.Name, IsCorrect = false};
                    foreach (var item in enPage.Data)
                    {
                        page.Data.Add(new WorkItem{Key = item.Key, RuText = "", EnText = item.Value});
                    }
                    LocData.Items.Add(page);
                }

                if (LocData.Items.Select(x => x.Name).Distinct().Count() != LocData.Items.Count)
                {
                    MessageBox.Show("Some Pages have same name!!!!", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }

                LocData.CurrentFile = new FileInfo(dlg.FileName).Name.Replace(PathDataExt,"");
            }
            OnPropertyChanged(nameof(LocData));
        }
        #endregion
    }
}