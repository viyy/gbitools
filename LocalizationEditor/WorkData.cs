﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using GBI.Scripts.Localization;
using LocalizationEditor.Annotations;

namespace LocalizationEditor
{
    public class WorkData
    {
        public string CurrentFile { get; set; }
        public ObservableCollection<WorkPage> Items { get; set; } = new ObservableCollection<WorkPage>();
    }

    public class WorkPage : INotifyPropertyChanged
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public ObservableCollection<WorkItem> Data { get; set; } = new ObservableCollection<WorkItem>();

        private bool _isCorrect;

        public bool IsCorrect
        {
            get => _isCorrect;
            set
            {
                if (value == _isCorrect) return;
                _isCorrect = value;
                OnPropertyChanged(nameof(IsCorrect));
            }
        }

        public bool IsDataCorrect => Data.All(x => x.IsCorrect) && ValidateKeys();

        private bool ValidateKeys()
        {
            return Data.Select(x => x.Key).Distinct().Count() == Data.Count;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class WorkItem
    {
        public string Key { get; set; }
        public string RuText { get; set; }
        public string EnText { get; set; }
        public bool IsCorrect => !string.IsNullOrEmpty(RuText) && !string.IsNullOrEmpty(EnText);
    }
}