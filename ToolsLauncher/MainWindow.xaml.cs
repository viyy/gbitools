﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GbiTools.Common;
using Newtonsoft.Json;

namespace ToolsLauncher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public ObservableCollection<Tool> Tools { get; set; } = new ObservableCollection<Tool>();

        public ICommand LaunchCommand { get; }

        public MainWindow()
        {
            LoadTools();
            InitializeComponent();
            LaunchCommand = new SimpleCommand(Launch);

            
        }

        private void Launch(object o)
        {
            if (!(o is string str )) return;
            Process.Start(str);
        }

        private void LoadTools()
        {
            try
            {
                var tools = JsonConvert.DeserializeObject<List<Tool>>(File.ReadAllText("tools.json"));
                Tools = new ObservableCollection<Tool>(tools);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error occured during loading tools. More information in err.log", "Error", MessageBoxButton.OK);
                File.AppendAllText("err.log", string.Format(Properties.Resources.MainWindow_LoadTools_ErrorMsg, DateTime.Now, e.Message));
            }
        }
    }
}
