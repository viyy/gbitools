﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using ToolsLauncher.Annotations;

namespace ToolsLauncher
{
    public class Tool : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string File { get; set; }
        public string VerInfoFile { get; set; }
        [JsonIgnore]
        private bool _isUpdateAvailable;
        [JsonIgnore]
        public bool IsUpdateAvailable
        {
            get => _isUpdateAvailable;
            set
            {
                if (value == _isUpdateAvailable) return;
                _isUpdateAvailable = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}