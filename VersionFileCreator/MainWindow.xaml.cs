﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GbiTools.Common;
using Microsoft.Win32;
using Nelfias.Crypto;
using Newtonsoft.Json;

namespace VersionFileCreator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ICommand DoWorkCommand { get; } 

        public MainWindow()
        {
            InitializeComponent();
            DoWorkCommand = new SimpleCommand(o=>DoWork());
        }

        private void DoWork()
        {
            var dlg = new OpenFileDialog
            {
                Filter = "All Files|*.*",
                Multiselect = true,
                CheckFileExists = true,
                InitialDirectory = Directory.GetCurrentDirectory()
            };
            if (dlg.ShowDialog() != true) return;
            var tools = dlg.FileNames.Select(fileName => new FileCheckSum {File = new FileInfo(fileName).Name, CheckSum = SHA1.GetHashFromFile(fileName)}).ToList();
            var output = JsonConvert.SerializeObject(tools);
            var sdlg = new SaveFileDialog
            {
                Filter = "JSON|*.json",
                AddExtension = true
            };
            if (sdlg.ShowDialog() != true) return;
            File.WriteAllText(sdlg.FileName, output);
        }
    }
}
